//
//  ViewController.m
//  CS441-HelloWorld
//
//  Created by Thomas Donohue on 1/30/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

static int countDown = 5;
static NSTimer *timer;

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.helloLabel.textAlignment = NSTextAlignmentCenter;
    
    self.helloLabel.text = [NSString stringWithFormat:@"%d", countDown];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)handleButtonPress:(id)sender {
    
    self.helloWorldLabel.text = @"Thomas Donohue";
    self.helloButton.enabled = NO;
    
    [self startCountDown];
}

- (void) startCountDown
{
    [self countDown];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                     target:self selector:@selector(countDownAnim:) userInfo:nil repeats:YES];
}

- (void) countDownAnim:(NSTimer *)timer
{
    [self countDown];
}

- (void) countDown
{
    CATransition *anim = [CATransition animation];
    anim.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    anim.type = kCATransitionPush;
    anim.subtype = kCATransitionFromTop;
    anim.duration = 0.4;
    [self.helloLabel.layer addAnimation:anim forKey:@"kCATransitionPush"];
    
    countDown--;
    
    if(countDown < 0){
        [self stopTimer];
        [self.helloLabel setFont:[UIFont fontWithName:@"Avenir Next Heavy" size:36]];
        self.helloLabel.text = @"Hello World!";
    }else{
        self.helloLabel.text = [NSString stringWithFormat:@"%d", countDown];
    }
    
}
- (void) stopTimer
{
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
    
}


@end
