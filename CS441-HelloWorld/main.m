//
//  main.m
//  CS441-HelloWorld
//
//  Created by Thomas Donohue on 1/30/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
