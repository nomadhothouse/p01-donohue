//
//  ViewController.h
//  CS441-HelloWorld
//
//  Created by Thomas Donohue on 1/30/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *helloLabel;

@property (strong, nonatomic) IBOutlet UIButton *helloButton;

@property (strong, nonatomic) IBOutlet UITextField *helloWorldLabel;

@end

