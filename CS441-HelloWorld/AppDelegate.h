//
//  AppDelegate.h
//  CS441-HelloWorld
//
//  Created by Thomas Donohue on 1/30/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

